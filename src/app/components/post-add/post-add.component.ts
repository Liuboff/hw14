import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/post';

import { PostService } from 'src/app/service/post.service';

@Component({
  selector: 'app-post-add',
  templateUrl: './post-add.component.html',
  styleUrls: ['./post-add.component.scss']
})
export class PostAddComponent implements OnInit {
  addForm = new FormGroup({
    title: new FormControl(''),
    body: new FormControl(''),
  });

  constructor(private postService: PostService, private router: Router) { }

  ngOnInit(): void { }

  onSubmit(data: Post) {
    if (this.addForm.valid) {
      this.postService.create(data.title, data.body)
        .subscribe({
          next: (res) => {
            this.router.navigate(['/posts']);
            console.log(res);
          },
          error: (e) => console.error(e)
        });

    }
  }
}
