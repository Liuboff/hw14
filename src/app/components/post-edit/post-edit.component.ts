import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

import { Post } from 'src/app/models/post';
import { PostService } from 'src/app/service/post.service';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.scss']
})
export class PostEditComponent implements OnInit {
  editForm = new FormGroup({
    title: new FormControl(''),
    body: new FormControl(''),
  });

  post?: Post;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private postService: PostService) { }

  ngOnInit(): void {
    const postId = this.route.snapshot.paramMap.get('id');
    this.postService.getOne(postId).subscribe(post => {
      this.post = post;
      this.editForm.setValue({
        title: this.post.title,
        body: this.post.body
      });
    });
  }

  onSubmit() {
    if (this.editForm.valid) {
      this.postService.update(this.post?.id, this.editForm.value).subscribe({
        complete: () => {
          this.router.navigate(['/posts']);
          console.log('Content updated successfully!');
        },
        error: (e) => {
          console.log(e);
        }
      });
    }
  }
}

