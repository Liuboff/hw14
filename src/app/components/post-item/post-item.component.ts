import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Post } from 'src/app/models/post';
import { Comment } from 'src/app/models/comment';
import { PostService } from 'src/app/service/post.service';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.scss']
})
export class PostItemComponent implements OnInit {

  constructor(private postService: PostService, private route: ActivatedRoute) { }
  post?: Post;
  comments?: Comment[];

  ngOnInit(): void {
    const postId = this.route.snapshot.paramMap.get('id');
    this.postService.getOne(postId)
      .subscribe({
        next: (post) => {
          this.post = post;
        },
        error: (e) => console.error(e)
      });
      this.postService.getComments(postId)
      .subscribe({
        next: (comments) => {
          this.comments = comments;
        },
        error: (e) => console.error(e)
      });
  }

}
