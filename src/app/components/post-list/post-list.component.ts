import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';

import { Post } from 'src/app/models/post';
import { PostService } from 'src/app/service/post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {
  
  posts?: Post[];
  pageSize: number = 10;
  currentPage: number = 0;
  postLength: number = 0; //???????????????????????????????????????????????

  constructor(private postService: PostService, private router: Router) { }

  ngOnInit(): void {
    this.postList();
  }

  postList(): void {
    this.postService.getPosts(this.pageSize, this.currentPage)
      .subscribe({
        next: (data) => {
          console.log(data);
          this.postLength = data.length; /////////////////////////////////
          this.posts = data;
        },
        error: (e) => console.error(e)
      });
  }

  onDelete(id: any) {
    this.postService.delete(id);
    this.posts = this.posts?.filter(post => post.id !== id)
  }

  onEdit(post: Post) {
    this.router.navigate([`posts-edit/${post.id}`], post.id);
  }

  OnPageChange(event: PageEvent){
    this.currentPage = event.pageIndex;
    this.pageSize = event.pageSize;
    this.postList();
  }

  toPost(post: Post){
    this.router.navigate([`posts/${post.id}`], post.id);
  }

}
















////////////////////////////////////////////////////////////////////////////////
// import { Component, OnInit } from '@angular/core';
// import { PageEvent } from '@angular/material/paginator';
// import { Router } from '@angular/router';

// import { Post } from 'src/app/models/post';
// import { PostService } from 'src/app/service/post.service';

// @Component({
//   selector: 'app-post-list',
//   templateUrl: './post-list.component.html',
//   styleUrls: ['./post-list.component.scss']
// })
// export class PostListComponent implements OnInit {
  
//   posts?: Post[];
//   postLength: number = 0;
//   pageSlice: any;

//   constructor(private postService: PostService, private router: Router) { }

//   ngOnInit(): void {
//     this.postList();
//   }

//   postList(): void {
//     this.postService.getAll()
//       .subscribe({
//         next: (data) => {
//           this.postLength = data.length;
//           this.posts = data;
//           this.pageSlice = this.posts?.slice(0, 10);
//         },
//         error: (e) => console.error(e)
//       });
//   }

//   onDelete(id: any) {
//     this.postService.delete(id);
//     this.posts = this.posts?.filter(post => post.id !== id)
//   }

//   onEdit(post: Post) {
//     this.router.navigate([`posts-edit/${post.id}`], post.id);
//   }

//   OnPageChange(event: PageEvent){
//     const startIndex = event.pageIndex * event.pageSize;
//     let endIndex = startIndex + event.pageSize;
//     if (endIndex > event.length) {
//       endIndex = event.length;
//     }
//     this.pageSlice = this.posts?.slice(startIndex, endIndex);
//   }

// }
