export interface Comment {
  id: any;
  name: string;
  email: string;
  body: string;
  postId: any;
}
