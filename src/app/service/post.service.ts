import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Post } from '../models/post';
import { Comment } from '../models/comment';
import { environment } from '../../environments/environment.prod';

const baseUrl = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Post[]> {
    return this.http.get<Post[]>(`${baseUrl}/posts`);
  }

  getPosts(pageSize: number, currentPage: number): Observable<Post[]> {
    let startPost = currentPage * pageSize;
    return this.http.get<Post[]>(`${baseUrl}/posts?_start=${startPost}&_limit=${pageSize}`);
  }

  getOne(id: any): Observable<Post> {
    return this.http.get<Post>(`${baseUrl}/posts/${id}`);
  }

  create(title: string, body: string) {
    let post = { title, body };
    return this.http.post<Post>(`${baseUrl}/posts`, post);
  }

  delete(id: any) {
    return this.http.delete<Post>(`${baseUrl}/posts/${id}`)
      .subscribe(() => {
        console.log('Deleted');
      });
  }

  update(id: any, post: Post) {
    return this.http.put<Post>(`${baseUrl}/posts/${id}`, post);
  }

  getComments(postId: any): Observable<Comment[]>{
    return this.http.get<Comment[]>(`${baseUrl}/posts/${postId}/comments`);
  }
}
